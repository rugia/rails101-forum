module GroupsHelper
    def render_group_title(title)
        simple_format(truncate(title, length: 15))
    end
end
